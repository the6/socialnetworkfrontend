import { Component, OnInit } from '@angular/core';
import { TransferService } from '../../services/transfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {

  constructor(private transferService: TransferService, private router: Router) { }

  ngOnInit() {
    console.log('hello');
  }

  myWall() {
    console.log('In myWall');
    this.transferService.setData(1);
    this.router.navigateByUrl('profile');
  }

}
