import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupService } from '../../services/signup.service';
import {User} from '../../models/user';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  newUser: User;

  constructor(private signupService: SignupService) { }

  ngOnInit() {
    console.log('In Init');
  }

  signup() {



    this.newUser = {
    id: -1,
    firstName: this.firstName,
    lastName: this.lastName,
    username: this.username,
    password: this.password,
    email: this.email
    };

    console.log(this.password);
    console.log(this.username);
    console.log(this.email);
    console.log(this.firstName);
    console.log(this.lastName);

    this.signupService.signup(this.newUser);

  }


}
