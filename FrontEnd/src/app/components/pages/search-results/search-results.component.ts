import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchbarService } from '../../../services/searchbar.service';
import { Router, NavigationEnd } from '@angular/router';
import { FriendRequestService } from '../../../services/friend-request-service.service';
import { LoginService } from '../../../services/login.service';
import { TransferService } from '../../../services/transfer.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnDestroy {

  navigationSubscription;

  constructor(private searchService: SearchbarService, private friendRequestService: FriendRequestService,
    private loginService: LoginService, private router: Router, private transferService: TransferService) {

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });
   }

  ngOnInit() {
    // this.searchService.getResults();
    if (!this.loginService.isLoggedIn()) {
      this.router.navigateByUrl('/login');
      return;
    }
  }

  initialiseInvites() {
    this.searchService.getResults();
  }

  addFriend(receiver: number) {
    this.friendRequestService.sendRequest(receiver);
  }

  friendWall(friendId: number) {
    this.transferService.setData(friendId);
    this.router.navigateByUrl('/profile');
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
       this.navigationSubscription.unsubscribe();
    }
  }
}
