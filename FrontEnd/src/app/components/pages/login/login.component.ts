import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../services/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';


  constructor(private loginService: LoginService, private router: Router) {

    if (this.loginService.isLoggedIn()) {
      this.router.navigateByUrl('/public');
    }

}

  ngOnInit() {
  }

  login() {
    const credentials = {
      username: this.username,
      password: this.password
    };
    this.loginService.login(credentials);

  }



}
