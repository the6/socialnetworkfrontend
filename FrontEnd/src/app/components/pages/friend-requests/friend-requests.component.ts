import { Component, OnInit, OnDestroy } from '@angular/core';
import { FriendRequestService } from '../../../services/friend-request-service.service';
import { Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-friend-requests',
  templateUrl: './friend-requests.component.html',
  styleUrls: ['./friend-requests.component.css']
})
export class FriendRequestsComponent implements OnInit, OnDestroy {

  navigationSubscription;

  constructor(private friendRequestService: FriendRequestService, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.friendRequestService.getFriendRequests();
      }
    });
  }

  ngOnInit() {
  //  this.friendRequestService.getFriendRequests();
  //  this.friendRequestService.getUsers();
  }

  approveRequest(sender: number) {
    this.friendRequestService.acceptRequest(sender);
  }

  denyRequest(sender: number) {
    this.friendRequestService.denyRequest(sender);
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
       this.navigationSubscription.unsubscribe();
    }
  }
}
