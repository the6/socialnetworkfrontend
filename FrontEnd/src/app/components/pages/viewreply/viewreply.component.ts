import { Component, OnInit } from '@angular/core';
import { TransferService } from '../../../services/transfer.service';
import { ReplyService } from '../../../services/reply.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewreply',
  templateUrl: './viewreply.component.html',
  styleUrls: ['./viewreply.component.css']
})
export class ViewreplyComponent implements OnInit {

  data = this.transferService.getData();
  constructor(private transferService: TransferService,   private router: Router
   , private replyService: ReplyService) {
    if (this.data) {
      this.loadReplies();
    } else {
      this.router.navigateByUrl('/public');
    }
  }

  ngOnInit() {
  }

  loadReplies() {
    this.replyService.loadReplies(this.data);
  }

}
