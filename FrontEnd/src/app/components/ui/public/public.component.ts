import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { LoginService } from '../../../services/login.service';
import { ReplyService } from '../../../services/reply.service';
import { TransferService } from '../../../services/transfer.service';
import {Post} from '../../../models/post';
import {Reply} from '../../../models/reply';
import { Router } from '@angular/router';


@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})


export class PublicComponent implements OnInit {

   buttonStatus;
   hidebutton: boolean[] = [];
   hidebuttondislike: boolean[] = [];

   hidden: boolean[] = [];
   comment: boolean[] = [];

   newReply: Reply;
   messageReply = '';



  // tslint:disable-next-line:max-line-length
  constructor(private publicService: PublicService,  private loginService: LoginService,  private replyService: ReplyService,  private router: Router,
            private transferService: TransferService) {

    if (this.loginService.isLoggedIn()) {
      this.router.navigateByUrl('/public');
    }
  }

  ngOnInit() {


    this.loadPost();

    this.hidden.fill(false, 0, this.publicService.posts.length - 1);
    this.comment.fill(true, 0, this.publicService.posts.length - 1);

  }


  loadPost() {
    this.publicService.loadPosts();


  }


  upVote(myPost: Post)  {
    myPost.ratio += 1;

    console.log(myPost.ratio);



    this.publicService.updateVote(myPost);
  }

  downVote(myPost: Post) {
    myPost.ratio -= 1;
    this.publicService.updateDislike(myPost);

  }





  follow(myNum: number) {
    console.log(this.hidebutton[myNum]);
    this.hidebutton[myNum] = true;
    console.log(this.hidebutton[myNum]);
    console.log('in follow');
  }

  followDislike(myNum: number) {
    this.hidebuttondislike[myNum] = true;
  }

  addComment(myNum: number) {

    console.log(this.hidden[myNum] );
    this.hidden[myNum] = true;
   this.comment[myNum] = false;
  }


  cancelComment(myNum: number) {
    console.log(this.comment[myNum] );
    this.hidden[myNum] = false;
    this.comment[myNum] = true;
  }

  myReply(myPost: Post, myNum: number) {
    console.log('in my reply');
    this.newReply = {
      id : -1,
      content : this.messageReply,
      author : null,
      post : myPost
     };

     this.hidden[myNum] = false;
     this.comment[myNum] = true;
     this.replyService.addReply(this.newReply);
  }

  viewReplies(myPost: Post) {
    console.log('in view reply');

    this.transferService.setData(myPost);
    this.router.navigateByUrl('reply');
  }





  trackByPost(id: number, post: Post): number { return post.id; }
}
