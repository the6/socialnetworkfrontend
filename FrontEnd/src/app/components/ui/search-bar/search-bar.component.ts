import { Component, OnInit } from '@angular/core';
import { SearchbarService } from '../../../services/searchbar.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  searchString: string;

  constructor(private searchService: SearchbarService) { }

  ngOnInit() {
  }

  search() {
    this.searchService.search(this.searchString);
  }
}
