import { Component, OnInit } from '@angular/core';
import { Post } from '../../../models/post';
import { MakePostService } from '../../../services/make-post.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-make-post',
  templateUrl: './make-post.component.html',
  styleUrls: ['./make-post.component.css']
})
export class MakePostComponent implements OnInit {

  buttonStatus;
  hidebutton: boolean;
  hidebuttondislike: boolean;

  hidden: boolean;
  comment: boolean;

  postContent: string;
  newPost: Post;

  constructor(private makePostService: MakePostService) { }

  ngOnInit() {
  }

  addComment() {
    console.log(this.hidden );
    this.hidden = true;
    this.comment = false;
  }

  cancelComment() {
    console.log(this.comment );
    this.hidden = false;
    this.comment = true;
  }

  makepost() {
    this.newPost = {
      id: -1,
      content: this.postContent,
      ratio: 0,
      submitted: new Date() , // this is an empty Date type
      type: 1,
      author: new User() // make this be the currently logged in person
    };
    // console.log(this.postContent);
    this.makePostService.makepost(this.newPost);
    this.cancelComment();
  }
}
