import { User } from './user';

export class FriendRequest {
    id: number;
    receiver: number;
    sender: number;
    status: number;
    senderWhole: User;
}
