import {User} from './user';
import {Post} from './post';
export class Reply {
    id: number;
    content: string;
    author: User;
    post: Post;
}
