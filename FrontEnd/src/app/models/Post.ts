import {User} from './user';
export class Post {
    id: number;
    content: String;
    author: User;
    submitted: Date;
    type: number;
    ratio: number;
}
