import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';
import { SearchResultsComponent } from './components/pages/search-results/search-results.component';
import { LoginComponent } from './components/pages/login/login.component';
import { UserpageComponent } from './components/userpage/userpage.component';
import { PublicComponent } from './components/ui/public/public.component';
import { FriendRequestsComponent } from './components/pages/friend-requests/friend-requests.component';
import { ViewreplyComponent } from './components/pages/viewreply/viewreply.component';
import { ProfileComponent } from './componets/profile/profile/profile.component';

const routes: Routes = [
  {path: 'search', component: SearchResultsComponent, runGuardsAndResolvers: 'always' },
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'public', component: PublicComponent},
  {path: 'requests', component: FriendRequestsComponent,  runGuardsAndResolvers: 'always'},
  {path: 'reply', component: ViewreplyComponent},
  {path: 'profile', component: ProfileComponent },
  {path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})
  ],
  exports: [
    RouterModule
  ]

})

export class AppRoutingModule { }
