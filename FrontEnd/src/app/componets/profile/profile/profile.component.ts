import { Component, OnInit } from '@angular/core';
import { TransferService } from '../../../services/transfer.service';
import { Router } from '@angular/router';
import { ProfileService } from '../../../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  data = this.transferService.getData();

  constructor(private transferService: TransferService,  private router: Router, private profileService: ProfileService)  {


    if (this.data) {
      this.checkUser();
    } else {
      this.router.navigateByUrl('public');
    }
   }

  ngOnInit() {
  }

  checkUser() {

    console.log('In profile ' + this.data);
    this.profileService.loadWallPost(this.data);
  }

}

