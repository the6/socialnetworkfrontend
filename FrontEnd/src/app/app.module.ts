import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SearchBarComponent } from './components/ui/search-bar/search-bar.component';
import { SearchResultsComponent } from './components/pages/search-results/search-results.component';
import { AppRoutingModule } from './app-routing.module';
import { SignupComponent } from './components/signup/signup.component';
import { RouterModule } from '@angular/router';
import {SignupService} from './services/signup.service';
import { LoginComponent } from './components/pages/login/login.component';
import { UserpageComponent } from './components/userpage/userpage.component';
import { PublicComponent } from './components/ui/public/public.component';
import { MakePostComponent } from './components/ui/make-post/make-post.component';
import { FriendRequestsComponent } from './components/pages/friend-requests/friend-requests.component';
import { ViewreplyComponent } from './components/pages/viewreply/viewreply.component';
import { ProfileComponent } from './componets/profile/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    SearchResultsComponent,
    SignupComponent,
    LoginComponent,
    UserpageComponent,
    PublicComponent,
    MakePostComponent,
    FriendRequestsComponent,
    ViewreplyComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    SignupService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
