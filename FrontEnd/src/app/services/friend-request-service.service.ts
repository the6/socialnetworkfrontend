import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { FriendRequest } from '../models/FriendRequest';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FriendRequestService {

  private url: string = environment.serverContext + '/request_friend/';
  public friendRequests: Array<FriendRequest>;

  constructor(private httpClient: HttpClient, private loginService: LoginService, private router: Router) { }

  getFriendRequests() {
    if (!this.loginService.isLoggedIn()) {
      this.router.navigateByUrl('/login');
      return;
    }

    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.friendRequests = new Array<FriendRequest>();
    this.httpClient.get(this.url + 'requests', httpOptions).subscribe((results: Array<FriendRequest>) => {
      results.forEach(r => this.friendRequests.push(r));
    });
  }

  getUsers() {
    // for (let i = 0; i < this.friendRequests.length; i++) {

    //   this.httpClient.get(environment.serverContext + '/users/' + this.friendRequests[i].sender)
    //     .subscribe((sender: User) => {this.friendRequests[i].senderWhole = sender; });
    // }
    this.friendRequests.forEach(fr => this.httpClient.get(environment.serverContext + '/users/' + fr.receiver)
    .subscribe((receiver: User) => {fr.senderWhole = receiver; }));
  }

  acceptRequest(sender: number) {
    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.get(environment.serverContext + '/friend/add/' + sender, httpOptions).subscribe( () => {});
  }

  denyRequest(sender: number) {
    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.get(environment.serverContext + '/friend/deny/' + sender, httpOptions).subscribe( () => {});
  }

  sendRequest(receiver: number) {
    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.get(this.url + receiver, httpOptions).subscribe(() => {});
  }
}
