import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchbarService {

  // Url for server app search function
  private url: string = environment.serverContext + '/search/';
  // Array to hold search results
  public searchResults: Array<User> = new Array<User>();
  searchString: string;

  constructor(private httpClient: HttpClient) { }

  // Retrieves an array of Users from the server app
  search(searchName: string) {
    this.searchString = searchName;
  }

  // Assigns search results to service field searchResults
  getResults() {
    this.searchResults = new Array<User>();
    this.httpClient.get(this.url + this.searchString).subscribe((results: Array<User>) => {
      results.forEach(r => this.searchResults.push(r));
    });
  }
}
