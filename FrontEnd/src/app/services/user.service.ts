import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: string = environment.serverContext + '/user/';

  constructor(private httpClient: HttpClient) { }

  getUserById(id: number) {
    this.httpClient.get(this.url + id).subscribe((result: User) => {
      return result;
    });
  }
}
