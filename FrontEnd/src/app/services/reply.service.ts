import { Injectable } from '@angular/core';
import {Reply} from '../models/reply';
import {Post} from '../models/post';
import { LoginService } from './login.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReplyService {

  public replies: Array<Reply> = new Array();

  constructor( private loginService: LoginService, private httpClient: HttpClient) { }


  addReply(newReply: Reply) {

    const url = 'http://localhost:8888/reply/';

    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.post(url, newReply, httpOptions)
      .subscribe(
        payload => {
          alert('Replied succesfully');
        },
        error => alert('Something went wrong')
      );

  }

  loadReplies(myPost: Post) {
    console.log('In reply service' + myPost.content);


    const url = `http://localhost:8888/reply/${myPost.id}`;

    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.get(url, httpOptions)
      .subscribe(payload => this.update(payload));

  }

  update(payload: any) {
    this.replies = payload;
    console.log(this.replies);
  }

}
