import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from '../models/post';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class MakePostService {

  constructor(private httpClient: HttpClient, private router: Router, private loginService: LoginService) { }

  makepost(newPost: Post) {
    const url = `http://localhost:8888/makepost/`;

    const token = this.loginService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Auth-Token': token
      })
    };

    this.httpClient.post(url, newPost, httpOptions)
         .subscribe(
           payload => {
             alert('creating post');
           },
           error => alert('Something went wrong: ' + error)
         );
  }

}
