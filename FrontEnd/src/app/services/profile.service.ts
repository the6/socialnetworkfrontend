import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpClient: HttpClient, private loginService: LoginService, private router: Router) { }

  public posts: Array<Post> = new Array();

  loadWallPost(userId: number) {
    const url = `http://localhost:8888/profile/${userId}/`;
    const token = this.loginService.getToken();
    const httpOptions = {
    headers: new HttpHeaders({
      'Auth-Token': token
    })
  };
    this.httpClient.get(url, httpOptions).subscribe(payload => this.update(payload));
  }

  update(payload: any) {
    this.posts = payload;
  }

}
