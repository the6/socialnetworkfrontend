import { TestBed, inject } from '@angular/core/testing';

import { MakePostService } from './make-post.service';

describe('MakePostService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MakePostService]
    });
  });

  it('should be created', inject([MakePostService], (service: MakePostService) => {
    expect(service).toBeTruthy();
  }));
});
