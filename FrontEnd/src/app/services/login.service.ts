import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private token: string = undefined;

    private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public loggedIn$ = this.loggedIn.asObservable();

    constructor(private httpClient: HttpClient, private router: Router) {
        this.checkAlreadyLoggedIn();
    }

    public isLoggedIn() {
        return this.token !== undefined;
    }

    private checkAlreadyLoggedIn() {
        const token = localStorage.getItem('token');
        if (token) {
            this.loggedIn.next(true);
            this.token = token;
            // Ideally, we should validate this token against the server too
        }
    }

    login(credentials: Object) {
        if (this.token) {
            alert('Already logged in!');
            return;
        }
        this.httpClient.post(`http://localhost:8888/login/`, credentials, {responseType: 'text'})
            .subscribe((payload: string) => {
                this.token = payload;
                if (this.token) {
                localStorage.setItem('token', this.token);
                this.loggedIn.next(true);
                this.router.navigateByUrl('/public');
                } else {

                  alert('Wrong username or password!');
                  this.token = undefined;
                  this.loggedIn.next(false);
                  localStorage.removeItem('token');
                  this.router.navigateByUrl('/login');

                }
        });
    }

    public getToken(): string {
        return this.token;
    }

    public logout(): void {
        this.token = undefined;
        this.loggedIn.next(false);
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
    }



}




