import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  public posts: Array<Post> = new Array();
  constructor(private httpClient: HttpClient, private loginService: LoginService, private router: Router) {
    if (this.loginService.isLoggedIn()) {
    this.router.navigateByUrl('/public');
    }  else {
  this.router.navigateByUrl('/login');
}}


  loadPosts(): void  {

    console.log('Inside in loadPosts');
    const url = `http://localhost:8888/public/`;

    this.httpClient.get(url).subscribe(payload => this.update(payload));
  }

  update(payload: any) {
    this.posts = payload;
    console.log(this.posts);
  }

  updateVote(myPost: Post) {

    const fakeToken = 'h';


    const url = `http://localhost:8888/public/up/${myPost.id}/`;
    this.httpClient.put(url, fakeToken).subscribe(payload => {});

    console.log('updateVote');
  }

  updateDislike(myPost: Post) {

    const fakeToken = 'h';

    const url = `http://localhost:8888/public/down/${myPost.id}`;

    this.httpClient.put(url, fakeToken).subscribe(payload => {});

  }





}
