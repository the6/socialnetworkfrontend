import { Injectable } from '@angular/core';
import {User} from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private httpClient: HttpClient,
    private router: Router
) { }


  signup(newUser: User) {

    console.log(newUser.firstName);
    const url = `http://localhost:8888/signup/`;
    this.httpClient.post(url, newUser)
        .subscribe(
          payload => {
            alert('Signed up successfully. Please log in to continue');
            this.router.navigateByUrl('/login');
          },
          error => alert('Something went wrong: ' + error)
        );


  }
}
